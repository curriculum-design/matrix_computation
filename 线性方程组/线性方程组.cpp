#include "pch.h"
#include <stdio.h>
#include <math.h>
#include "matrix.h"

void getmatrix(matrix &A)
{
	for (unsigned int i = 0; i < A.getr(); i++)
	{
		for (unsigned int j = 0; j < A.getc(); j++)
		{
			scanf_s("%lf", &A.m[i][j]);
		}
	}
	printf("矩阵如下\n");
	A.output();
}

void getvector(vector &B)
{
	for (unsigned int i = 0; i < B.getl(); i++)
	{
		scanf_s("%lf", &B.v[i]);
	}
	printf("向量如下\n");
	B.output();
}

void linerSolve()
{
	unsigned int r;

	printf("请输入方阶数：");
	scanf_s("%d", &r);

	matrix A = matrix(r, r);
	vector B = vector(r);
	printf("请按行从左到右依次输入系数矩阵，不同元素用空格隔开\n");
	getmatrix(A);
	printf("请按从上到下依次输入常数向量，不同元素用空格隔开\n");
	getvector(B);

	printf("结果为：\n");
	matrix::solve(A, B).output();
}

void matMul()
{
	unsigned int r,c;
	printf("请输入矩阵1的行列数，空格分隔：");
	scanf_s("%d%d", &r, &c);
	matrix m1 = matrix(r, c);
	printf("请按行从左到右依次输入矩阵，不同元素用空格隔开\n");
	getmatrix(m1);
	printf("请输入矩阵2的行列数，空格分隔：");
	scanf_s("%d%d", &r, &c);
	printf("请按行从左到右依次输入矩阵，不同元素用空格隔开\n");
	matrix m2 = matrix(r, c);
	getmatrix(m2);

	printf("结果为：\n");
	m1.dot(m2).output();
}


int main()
{
	linerSolve();
}